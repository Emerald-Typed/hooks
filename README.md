<a name="readme-top"></a>
<br />

<div align="center">
<a href="https://emerald-typed.vercel.app/">
<img src="https://gitlab.com/Emerald-Typed/readme/-/raw/main/public/LOGO.png" alt="Logo">
</a>

<h3 align="center">Typescript React Hooks</h3>

<p align="center">
Next.js Page w/ Typescript React Hook Examples by Difficulty
<br />
<a href="https://emerald-typed-hooks.vercel.app/"><strong>View Deployment  »</strong></a>
<br />
<a href="https://gitlab.com/Emerald-Typed/hooks"><strong>Explore the Repo »</strong></a>
<br />
<a href="https://emerald-typed.vercel.app/contact">Contact Me</a>
·
<a href="https://gitlab.com/Emerald-Typed/hooks/issues">Report Bug</a>
·
<a href="https://gitlab.com/Emerald-Typed/hooks/issues">Request Feature</a>
</p>

[![Last Commit][commit-badge]](https://gitlab.com/Emerald-Typed/hooks)

</div>

## Table of Contents

#### [About](#about-the-project)

#### [ScreenShot](#screenshot)

#### [Libraries](#built-with)

#### [Installation](#installation)

#### [Usage](#usage)

#### [Contact](#Contact)

#### [License](#license)

## About The Project

### ScreenShot

![Project Screen Shot][Screenshot]

This repository contains a Next.js application designed to dynamically load typed components of React hooks. Each component showcases a working example along with its corresponding code, all accessible through dynamic page routes.

### Prerequisites

Required libraries and how to install them.

[![Node][Node.js]][Node-url]

[![React][React.js]][React-url]

[![NPM][NPM]][NPM-url]

### Built With

[![Next.js](https://img.shields.io/badge/next.js-34d399?logo=next.js&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://nextjs.org/)
[![React](https://img.shields.io/badge/react-34d399?logo=react&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://reactjs.org/)
[![TypeScript](https://img.shields.io/badge/typescript-34d399?logo=typescript&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://www.typescriptlang.org/)
[![Tailwind](https://img.shields.io/badge/tailwindcss-34d399?logo=tailwindcss&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://tailwindcss.com/)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Setup and Use

### Installation

1. **Clone the Repository:**

```
git clone https://gitlab.com/Emerald-Typed/hooks.git
```

2. **Navigate to the root directory:**

```
cd /hooks
```

3. **Install Dependencies:**

```
npm install
```

### Usage

1. **Start the development server:**

```
npm run dev
```

**Open the browser and navigate to [http://localhost:3000](http://localhost:3000)**

or

**Open the browser and navigate to [Deployed Page](https://emerald-typed-hooks.vercel.app/)**

</br>

_For an example, please refer to the [Documentation](https://react.dev/reference/react/hooks)_

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Contact

Kyle Hipple - [Emerald-Typed Gitlab](https://gitlab.com/Emerald-Typed) - [Contact Me](https://emerald-typed.vercel.app/contact)

Project Link: [https://gitlab.com/Emerald-Typed/hooks](https://gitlab.com/Emerald-Typed/hooks)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## License

All Rights Reserved License. See [License.txt](https://gitlab.com/Emerald-Typed/hooks/-/raw/main/LICENSE.txt)
for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>
 
[commit-badge]: https://img.shields.io/gitlab/last-commit/Emerald-Typed/hooks?label=last%20commit&style=for-the-badge&color=34d399
[NPM]: https://img.shields.io/badge/npm-34d399?logo=npm&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge
[NPM-url]: https://www.npmjs.com/
[Node.js]: https://img.shields.io/badge/node.js-34d399?logo=node.js&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge
[Node-url]: https://nodejs.org/en/
[React.js]: https://img.shields.io/badge/react-34d399?logo=react&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge
[React-url]: https://reactjs.org/
[Screenshot]: https://gitlab.com/Emerald-Typed/hooks/-/raw/main/public/screenshot.png
