import Link from "next/link"
import { ReactNode } from "react"
export default function Home() {
  const UnderDevelopement: React.FC<{ children: ReactNode }> = ({
    children,
  }) => (
    <span className="devButton">
      {children}
      <div className="flex justify-center container">
        <span className="tooltiptext">Under Development</span>
      </div>
    </span>
  )
  return (
    <div className="flex flex-col mx-auto min-w-[60%] max-w-7xl my-auto">
      <h1 className="loud-text md:text-left sm:text-center">
        Welcome to my React Hooks Page
      </h1>
      <br />
      <h2 className="text-2xl font-semibold mb-2 text-center">
        React Hooks by difficulty with examples to reference:
      </h2>
      <ul className="flex text-center mt-8 text-2xl text-xl sm:flex-row flex-col">
        <Link className="purpleButtons " href="/Beginner">
          Beginner Hooks
        </Link>
        <UnderDevelopement>
          <Link href="/" className="cursor-not-allowed">
            Intermediate Hooks
          </Link>
        </UnderDevelopement>

        <UnderDevelopement>
          <Link href="/" className="cursor-not-allowed">
            Advanced Hooks
          </Link>
        </UnderDevelopement>
      </ul>
    </div>
  )
}
