import Link from "next/link"

const AdvancedPage = () => {
  const links = [
    { href: "/Advanced/useLayoutEffect", title: "useLayoutEffect" },
    { href: "/Advanced/useInsertionEffect", title: "useInsertionEffect" },
    { href: "/Advanced/useImperativeHandle", title: "useImperativeHandle" },
    { href: "/Advanced/useId", title: "useId" },
    { href: "/Advanced/useDebugValue", title: "useDebugValue" },
    { href: "/Advanced/useDeferredValue", title: "useDeferredValue" },
    { href: "/Advanced/useSyncExternalStore", title: "useSyncExternalStore" },
    { href: "/Advanced/customHooks", title: "Custom Hooks" },
  ]
  return (
    <div className="mx-auto min-w-[60%] max-w-7xl my-auto">
      <h1 className="loud-text">Advanced Hooks</h1>
      <div className="border-b p-4 border-emerald-400">
        <ul className="flex flex-col  font-medium gap-8 text-2xl">
          {links.map((link, index) => (
            <li key={index}>
              <Link href={link.href}>{link.title}</Link>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}

export default AdvancedPage
