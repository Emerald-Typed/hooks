"use client"

import { usePathname } from "next/navigation"

export default function HookCard() {
  const path = usePathname()
  const hookId = path.split("/")[2]
  let content
  switch (hookId) {
    case "example":
      content = HookName()
      break
    default:
      content = <div>{hookId}</div>
  }
  return (
    <div className="mx-auto max-w-7xl my-auto sm:w-full min-w-[60%]">
      <h1 className="loud-text py-4">{hookId}</h1>
      {content}
    </div>
  )
}

function HookName() {
  const Component = () => {
    return <div className="flex flex-col items-center">words</div>
  }
  const contents = `const DemoComponent = () => {
    const [state, setState] = useState(0)

    return (
      <div>
        <p>Placeholder: {state}</p>
      </div>
    )
  }`

  return (
    <div className="min-w-[60%]">
      <div className="mb-8 flex flex-col">
        <h2 className="text-2xl font-semibold mb-2">Component Code:</h2>
        <pre className="bg-gray-900 text-white p-4 rounded-md">
          <code className="language-tsx">{contents}</code>
        </pre>
      </div>
      <div className="mb-8 flex flex-col">
        <h2 className="text-2xl font-semibold mb-2">Live Demo:</h2>
        <div className="mx-auto bg-gray-900 text-white p-4 rounded-md text-lg w-full">
          <Component />
        </div>
      </div>
    </div>
  )
}
