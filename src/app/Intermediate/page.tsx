import Link from "next/link"

const IntermediatePage = () => {
  const links = [
    { href: "/Intermediate/useMemo", title: "useMemo" },
    // { href: "/Intermediate/useCallback", title: "useCallback" },
    // { href: "/Intermediate/useReducer", title: "useReducer" },
    // { href: "/Intermediate/useTransition", title: "useTransition" },
    // {
    //   href: "/Intermediate/useOptimistic",
    //   title: "useOptimistic (only in React Canary and experimental builds)",
    // },
  ]
  return (
    <div className="mx-auto min-w-[60%] max-w-7xl my-auto">
      <h1 className="loud-text">Intermediate Hooks</h1>
      <div className="border-b p-4 border-emerald-400">
        <ul className="flex flex-col  font-medium gap-8 text-2xl">
          {links.map((link, index) => (
            <li key={index}>
              <Link href={link.href}>{link.title}</Link>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}

export default IntermediatePage
