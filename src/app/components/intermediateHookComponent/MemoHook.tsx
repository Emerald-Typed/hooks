// import { useMemo } from "react"

export default function Memo() {
  const MemoComponent = () => {
    return (
      <div className="flex flex-col items-center text-base">
        <h2 className="text-2xl font-semibold mb-2 text-violet-400">
          Example useMemo:
        </h2>
        <button className="bg-violet-400 my-2 lg:w-1/2 w-full rounded-md p-2">
          Memo Component
        </button>
      </div>
    )
  }

  const useMemoContents = `import { useMemo } from "react"
  export default function Memo() {
    const memoComponent = () => {
      return (
        <div className="flex flex-col items-center text-base">
          <h2 className="text-2xl font-semibold mb-2 text-violet-400">
            Example useState:
          </h2>
          <button
            onClick={colorswap}
          >
            Memo Component
          </button>
        </div>
      )
    }
  `
  const useMemoUseCase = [
    "When you have a function that is computationally expensive and you want to cache the result of that function so that it is not recomputed on every render.",
  ]
  const useMemoDocs = "https://react.dev/reference/react/useMemo"

  return { MemoComponent, useMemoContents, useMemoUseCase, useMemoDocs }
}
