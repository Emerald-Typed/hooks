import { ReactElement } from "react"
import Image, { StaticImageData } from "next/image"
import Link from "next/link"

import GitLabImage from "/public/gitlab.png"
import LinkedInImage from "/public/linkedin.svg"
import PortfolioImage from "/public/static/diamondW.png"
import ThingyImage from "/public/thingiverse.png"

export default function AccountsBar() {
  return (
    <nav className="bg-violet-400/50 shadow-lg pt-1">
      <div className="mx-auto text-center sm:px-6 lg:px-8">
        <div className="text-2xl leading-9 tracking-tight font-bold text-white">
          My Accounts:
        </div>
        <div className="lg:space-x-10 lg:flex lg:justify-center grid grid-cols-2 m-2 text-white pb-2">
          <AccountLink
            href="https://gitlab.com/Emerald-Typed"
            label="GitLab"
            image={GitLabImage}
          />
          <AccountLink
            href="https://www.linkedin.com/in/kyle-hipple"
            label="LinkedIn"
            image={LinkedInImage}
          />
          <AccountLink
            href="https://emerald-typed.vercel.app/"
            label="Portfolio"
            image={PortfolioImage}
          />
          <AccountLink
            href="https://www.thingiverse.com/emerald_typed/designs"
            label="Thingiverse"
            image={ThingyImage}
          />
        </div>
      </div>
    </nav>
  )
}

function AccountLink({
  href,
  label,
  image,
}: {
  href: string
  label: string
  image: StaticImageData
}): ReactElement {
  return (
    <Link
      href={href}
      rel="noopener noreferrer"
      target="_blank"
      className="inline-flex bg-gradient-to-r from-violet-400/50 to-purple-400/75 items-center rounded-md hover:font-semibold hover:shadow-md m-1 p-2"
    >
      <Image src={image} alt={label} className="mr-2 h-6 w-6" />
      {label}
    </Link>
  )
}
