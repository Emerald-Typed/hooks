"use client"

import { useState, ReactElement, ReactNode, ReactEventHandler } from "react"
import Image from "next/image"
import { usePathname } from "next/navigation"
import Link from "next/link"
import Accounts from "./account"

export default function NewNav(): ReactElement {
  const pathName = usePathname()
  const [isOpen, setIsOpen] = useState<boolean>(false)

  const handleBurger: ReactEventHandler = () => {
    setIsOpen(!isOpen)
  }

  return (
    <>
      <div className="bg-gradient-to-r from-emerald-400 via-violet-400 to-purple-400 sticky top-0 w-full">
        <nav className="bg-emerald-400/75">
          <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
            <div className="flex justify-between h-16 items-center font-medium">
              <button
                className="sm:hidden p-2 rounded-md bg-emerald-500 text-white"
                onClick={handleBurger}
              >
                {isOpen ? (
                  <svg
                    className="block h-6 w-6 text-white"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M6 18L18 6M6 6l12 12"
                    />
                  </svg>
                ) : (
                  <svg
                    className="block h-6 w-6 text-white"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
                    />
                  </svg>
                )}
              </button>
              <div className="flex-shrink-0">
                <Link href="/">
                  <div className="flex items-center rounded-md p-1 m-1 text-white block text-white hover:shadow-md hover:font-semibold">
                    <div className="hidden sm:block">
                      <Image
                        width={40}
                        height={40}
                        src="/static/diamondW.png"
                        alt="Home"
                      />
                    </div>
                    <div className="block sm:hidden">
                      <Image
                        height={50}
                        width={70}
                        src="/static/Emerald-ETW.png"
                        alt="Home"
                      />
                    </div>
                    <h1 className="text-white ml-5 hidden sm:block">
                      Kyle Hipple
                    </h1>
                  </div>
                </Link>
              </div>
              <div className="hidden sm:flex space-x-4">
                <NavLink pathName={pathName} href="/Beginner">
                  Beginner
                </NavLink>
                <Link className="devButton" href="/">
                  Intermediate
                </Link>

                <Link className="devButton" href="/">
                  Advanced
                </Link>
              </div>
            </div>
          </div>
          {isOpen && (
            <div className="sm:hidden pb-5" id="mobile-menu">
              <div className="px-4 space-y-2">
                <NavLink pathName={pathName} href="/Beginner">
                  Beginner
                </NavLink>
                <Link className="devButton" href="/">
                  Intermediate
                </Link>
                <Link className="devButton" href="/">
                  Advanced
                </Link>
              </div>
            </div>
          )}
        </nav>
      </div>
      <Accounts />
    </>
  )
}

function NavLink({
  pathName,
  href,
  children,
}: {
  pathName: string
  href: string
  children: ReactNode
}): ReactElement {
  const isActive =
    pathName === href
      ? "block p-3 m-2 bg-gradient-to-r from-violet-300/10 to-purple-400/75 text-white shadow-md font-semibold hover:bg-gradient-to-r from-emerald-300/75 to-teal-400/75"
      : "block p-3 m-2 text-white hover:shadow-sm hover:font-semibold hover:bg-gradient-to-r from-teal-400/50 to-teal-400/20"
  return (
    <a href={href} className={`rounded-md p-1 m-1 ${isActive}`}>
      {children}
    </a>
  )
}
