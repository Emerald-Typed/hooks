import { createContext, useContext, ReactNode } from "react"

export default function Context() {
  type ThemeValue = string
  const ThemeContext = createContext("bg-emerald-200 text-purple-900")
  //passed to ComponentuseContext
  function ThemeProvider({
    children,
    value,
  }: {
    children: ReactNode
    value: ThemeValue
  }) {
    return (
      <ThemeContext.Provider value={value}>
        <div className={`flex flex-col items-center mt-2 rounded-md ${value}`}>
          {children}
        </div>
      </ThemeContext.Provider>
    )
  }
  function ComponentnoContext() {
    return (
      <>
        <p>Component No Context // Theme: null</p>
      </>
    )
  }
  function ComponentwithProviderContext() {
    const theme = useContext(ThemeContext)
    return (
      <>
        <p>Component with Context Provider // Theme: {theme}</p>
      </>
    )
  }
  function ComponentuseContext() {
    const theme = useContext(ThemeContext)
    return (
      <div className={`flex flex-col items-center rounded-md mt-2 ${theme}`}>
        <p>Component with simple useContext // Theme: {theme}</p>
      </div>
    )
  }
  const UseContextComponent = () => {
    return (
      <div className="text-center">
        <h2 className="text-2xl font-semibold mb-2 text-violet-400">
          Example useContext:
        </h2>
        <ComponentnoContext />
        <ThemeProvider value="bg-emerald-200 text-black">
          <ComponentwithProviderContext />
        </ThemeProvider>
        <ComponentuseContext />
      </div>
    )
  }

  const useContextContents = `import { createContext, useContext, ReactNode } from "react"

  export default function Context() {
    type ThemeValue = string
    const ThemeContext = createContext("bg-emerald-200 text-purple-900")
    //passed to ComponentuseContext
    function ThemeProvider({
      children,
      value,
    }: {
      children: ReactNode
      value: ThemeValue
    }) {
      return (
        <ThemeContext.Provider value={value}>
          <div className={\`flex flex-col items-center mt-2 rounded-md \${value}\`}>
            {children}
          </div>
        </ThemeContext.Provider>
      )
    }
    function ComponentnoContext() {
      return (
        <>
          <p>Component No Context // Theme: null</p>
        </>
      )
    }
    function ComponentwithProviderContext() {
      const theme = useContext(ThemeContext)
      return (
        <>
          <p>Component with Context Provider // Theme: {theme}</p>
        </>
      )
    }
    function ComponentuseContext() {
      const theme = useContext(ThemeContext)
      return (
        <div className={\`flex flex-col items-center rounded-md mt-2 \${theme}\`}>
          <p>Component with simple useContext // Theme: {theme}</p>
        </div>
      )
    }
    const UseContextComponent = () => {
      return (
        <div className="text-center">
          <h2 className="text-2xl font-semibold mb-2 text-violet-400">
            Example useContext:
          </h2>
          <ComponentnoContext />
          <ThemeProvider value="bg-emerald-200 text-black">
            <ComponentwithProviderContext />
          </ThemeProvider>
          <ComponentuseContext />
        </div>
      )
    }`
  const useContextUseCase = [
    "Global State Management: Using useContext to manage global state in your React application. Share state or functions across components without prop drilling.",
    "Theme Switching: Storing and updating the current theme throughout your application using a context provider and useContext hook.",
  ]
  const useContextDocs = "https://reactjs.org/docs/context.html"

  return {
    UseContextComponent,
    useContextContents,
    useContextUseCase,
    useContextDocs,
  }
}
