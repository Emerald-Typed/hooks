import React, { useState, useEffect } from "react"

export default function Effect() {
  const UseEffectComponent = () => {
    const [count, setCount] = useState<number>(0)
    const [countContinuously, setCountContinuously] = useState<number>(0)

    const NonChangingValue: string =
      "I am a value and I firmly beleive I will constantly stay the same!"

    useEffect(() => {
      let timer = setTimeout(() => {
        setCount((prevCount: number) => prevCount + 1)
      }, 1000)

      return () => clearTimeout(timer)
    }, [NonChangingValue]) //dependency array makes it only run on intial render however if a value in that array changes then the effect-callback function will run again.

    useEffect(() => {
      let continuousTimer = setTimeout(() => {
        setCountContinuously((prevCount: number) => prevCount + 1)
      }, 1000)

      return () => clearTimeout(continuousTimer)
    }) //Without a dependency array the callback runs after every rendering.

    return (
      <div className="text-center text-violet-400">
        <h2 className="text-2xl font-semibold mb-2 text-violet-400">
          Example useEffect:
        </h2>
        <h1>First useEffect rendered {count} times!</h1>
        <h1>Second useEffect rendered {countContinuously} times!</h1>
      </div>
    )
  }
  const useEffectContents = `import React, { useState, useEffect } from "react"

  export default function Effect() {
    const UseEffectComponent = () => {
      const [count, setCount] = useState<number>(0)
      const [countContinuously, setCountContinuously] = useState<number>(0)
  
      const NonChangingValue: string =
        "I am a value and I firmly beleive I will constantly stay the same!"
  
      useEffect(() => {
        let timer = setTimeout(() => {
          setCount((prevCount: number) => prevCount + 1)
        }, 1000)
  
        return () => clearTimeout(timer)
      }, [NonChangingValue]) //dependency array makes it only run on intial render however if a value in that array changes then the effect-callback function will run again.
  
      useEffect(() => {
        let continuousTimer = setTimeout(() => {
          setCountContinuously((prevCount: number) => prevCount + 1)
        }, 1000)
  
        return () => clearTimeout(continuousTimer)
      }) //Without a dependency array the callback runs after every rendering.
  
      return (
        <div className="text-center text-violet-400">
          <h2 className="text-2xl font-semibold mb-2 text-violet-400">
            Example useEffect:
          </h2>
          <h1>First useEffect rendered {count} times!</h1>
          <h1>Second useEffect rendered {countContinuously} times!</h1>
        </div>
      )
    }`
  const useEffectUseCase = [
    "Connecting to an External System: establish a connection or subscription to an external system, such as a WebSocket, messaging queue, or external API.",
    "Subscription Management: Setting up and cleaning up subscriptions.",
    "Document Title Update: Dynamically updating the document title based on component state.",
    "Side Effects: Handling side effects like logging, analytics, etc.",
    "Timer or Interval: Implementing a timer or interval functionality using useEffect. For example, updating a component state at regular intervals or scheduling periodic tasks.",
  ]
  const useEffectName = "useEffect"
  const useEffectDocs = "https://react.dev/reference/react/useEffect"
  return {
    UseEffectComponent,
    useEffectContents,
    useEffectUseCase,
    useEffectName,
    useEffectDocs,
  }
}
