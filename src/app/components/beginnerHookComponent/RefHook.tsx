import { useRef } from "react"

type UseRefForm = {
  name: HTMLInputElement | null
  email: HTMLInputElement | null
  message: HTMLTextAreaElement | null
}

export default function Ref() {
  const UseRefComponent = () => {
    const formRefs = useRef<UseRefForm>({
      name: null,
      email: null,
      message: null,
    })

    const handleSubmit = (event: React.FormEvent) => {
      event.preventDefault()

      const name = formRefs.current?.name?.value || ""
      const email = formRefs.current?.email?.value || ""
      const message = formRefs.current?.message?.value || ""

      // Log form data to the console (in a real app, I would send this data to a server)
      console.log("Name:", name)
      console.log("Email:", email)
      console.log("Message:", message)
      //check if the refs are not null before resetting
      if (formRefs.current?.name) formRefs.current.name.value = ""
      if (formRefs.current?.email) formRefs.current.email.value = ""
      if (formRefs.current?.message) formRefs.current.message.value = ""
    }

    return (
      <div className="flex flex-col mx-auto items-center text-violet-400 text-center max-w-4xl">
        <h2 className="text-2xl font-semibold mb-2 text-violet-400">
          Example useRef Form:
        </h2>
        <form
          onSubmit={handleSubmit}
          className="flex-col flex w-full  text-base "
        >
          <label>
            Name:
            <input
              type="text"
              ref={(e: HTMLInputElement | null) => (formRefs.current.name = e)}
              required
              className="w-full bg-violet-200 rounded-md p-2"
            />
          </label>
          <br />
          <label>
            Email:
            <input
              type="email"
              ref={(element: HTMLInputElement | null) =>
                (formRefs.current.email = element)
              }
              required
              className="w-full bg-violet-200 rounded-md p-2"
            />
          </label>
          <br />
          <label>
            Message:
            <textarea
              ref={(element: HTMLTextAreaElement | null) =>
                (formRefs.current.message = element)
              }
              required
              className="w-full bg-violet-200 rounded-md p-2"
            />
          </label>
          <br />
          <button
            className="mx-auto text-white bg-violet-200 my-2 lg:w-1/2 w-full rounded-md p-2 hover:bg-violet-300/80"
            type="submit"
          >
            Submit
          </button>
        </form>
      </div>
    )
  }
  const useRefContents = `import { useRef, useEffect } from "react"

  type useRefForm = {
    name: string
    email: string
    message: string
  }
  
  export default function Ref() {
    const UseRefComponent = () => {
      // useRef for form input values
      const nameRef = useRef<HTMLInputElement>(null)
      const emailRef = useRef<HTMLInputElement>(null)
      const messageRef = useRef<HTMLTextAreaElement>(null)
  
      const handleSubmit = (event: React.FormEvent) => {
        event.preventDefault()
  
        // Get input values using useRef
        const name = nameRef.current?.value || ""
        const email = emailRef.current?.value || ""
        const message = messageRef.current?.value || ""
  
        // Log form data to the console (in a real app, I would send this data to a server)
        console.log("Name:", name)
        console.log("Email:", email)
        console.log("Message:", message)
        //check if the refs are not null before resetting
        if (nameRef.current) nameRef.current.value = ""
        if (emailRef.current) emailRef.current.value = ""
        if (messageRef.current) messageRef.current.value = ""
      }
  
      return (
        <div className="flex flex-col mx-auto items-center text-violet-400 text-center max-w-4xl">
          <h2 className="text-2xl font-semibold mb-2 text-violet-400">
            Example useRef Form:
          </h2>
          <form
            onSubmit={handleSubmit}
            className="flex-col flex w-full  text-base "
          >
            <label>
              Name:
              <input
                type="text"
                ref={nameRef}
                required
                className="w-full bg-violet-200 rounded-md p-2"
              />
            </label>
            <br />
            <label>
              Email:
              <input
                type="email"
                ref={emailRef}
                required
                className="w-full bg-violet-200 rounded-md p-2"
              />
            </label>
            <br />
            <label>
              Message:
              <textarea
                ref={messageRef}
                required
                className="w-full bg-violet-200 rounded-md p-2"
              />
            </label>
            <br />
            <button
              className="mx-auto text-white bg-violet-200 my-2 lg:w-1/2 w-full rounded-md p-2 hover:bg-violet-300/80"
              type="submit"
            >
              Submit
            </button>
          </form>
        </div>
      )
    }`
  const useRefUseCase = [
    "DOM Manipulation: Utilizing useRef to interact with the DOM directly. For example, focusing on an input field, measuring the dimensions of an element, or triggering imperative animations.",
    "Persistent Values: Storing values that persist across renders without causing re-renders. Useful for keeping track of mutable values without triggering component updates.",
    "Changing Values Without Triggering Re-renders: Storing values that persist across renders without causing re-renders. Useful for keeping track of mutable values without triggering component updates.",
  ]
  const useRefDocs = "https://react.dev/reference/react/useRef"
  return { UseRefComponent, useRefContents, useRefUseCase, useRefDocs }
}
