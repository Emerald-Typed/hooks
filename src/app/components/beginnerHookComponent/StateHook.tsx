import { useState } from "react"

type StatefulValues = {
  GreenTheme: boolean
  greenState: string
  baseState: string
}

export default function State() {
  const StatefulComponent = () => {
    const [colorstate, setColorState] = useState<StatefulValues>({
      GreenTheme: true,
      greenState: "bg-violet-400",
      baseState: "bg-emerald-300",
    })
    const colorswap = () => {
      setColorState({ ...colorstate, GreenTheme: !colorstate.GreenTheme })
    }

    const bgcolor = colorstate.GreenTheme
      ? colorstate.greenState
      : colorstate.baseState

    return (
      <div className="flex flex-col items-center text-base">
        <h2 className="text-2xl font-semibold mb-2 text-violet-400">
          Example useState:
        </h2>
        <p
          className={`text-center my-2 lg:w-1/2 w-full rounded-md p-2 ${bgcolor}`}
        >
          Tailwind Color: {bgcolor}
        </p>
        <button
          className="bg-violet-400 my-2 lg:w-1/2 w-full rounded-md p-2"
          onClick={colorswap}
        >
          Change Color
        </button>
      </div>
    )
  }

  const useStateContents = `import { useState } from "react"

  type StatefulValues = {
    GreenTheme: boolean
    greenState: string
    baseState: string
  }
  
  export default function State() {
    const StatefulComponent = () => {
      const [colorstate, setColorState] = useState<StatefulValues>({
        GreenTheme: true,
        greenState: "bg-violet-400",
        baseState: "bg-emerald-300",
      })
      const colorswap = () => {
        setColorState({ ...colorstate, GreenTheme: !colorstate.GreenTheme })
      }
  
      const bgcolor = colorstate.GreenTheme
        ? colorstate.greenState
        : colorstate.baseState
  
      return (
        <div className="flex flex-col items-center text-base">
          <h2 className="text-2xl font-semibold mb-2 text-violet-400">
            Example useState:
          </h2>
          <p
            className={\`text-center my-2 lg:w-1/2 w-full rounded-md p-2 \${bgcolor}\`}
          >
            Tailwind Color: {bgcolor}
          </p>
          <button
            className="bg-violet-400 my-2 lg:w-1/2 w-full rounded-md p-2"
            onClick={colorswap}
          >
            Change Color
          </button>
        </div>
      )
    }
  `
  const useStateUseCase = [
    "Counter (Number): a count state variable holds a number that can be incremented",
    "Text Field(String): a text state variable holds an input string that can be changed most likely from a form",
    "Checkbox (Boolean): a selected or theme state variable could a boolean",
    "Typed Array (Array): a list of items that can be added to or removed from could  also be tuple",
  ]
  const useStateDocs = "https://react.dev/reference/react/useState"

  return { StatefulComponent, useStateContents, useStateUseCase, useStateDocs }
}
