"use client"

import { usePathname } from "next/navigation"
import { CodeBlock, atomOneLight } from "react-code-blocks"
import Link from "next/link"
import State from "@/app/components/beginnerHookComponent/StateHook"
import Effect from "@/app/components/beginnerHookComponent/EffectHook"
import Context from "@/app/components/beginnerHookComponent/ContextHook"
import Ref from "@/app/components/beginnerHookComponent/RefHook"

import React from "react"

type HookCardType = {
  Name: string
  Docs: string
  HookComponent: React.FC
  contents: string
  useCase?: string[]
}

export default function HookCard() {
  const path = usePathname()
  const hookId = path.split("/")[2]
  let content

  switch (hookId) {
    case "useState":
      const {
        StatefulComponent,
        useStateContents,
        useStateUseCase,
        useStateDocs,
      } = State()
      const useStateDetails = {
        HookComponent: StatefulComponent,
        contents: useStateContents,
        useCase: useStateUseCase,
        Docs: useStateDocs,
        Name: hookId,
      }
      content = displayHookContents(useStateDetails)
      break
    case "useEffect":
      const {
        UseEffectComponent,
        useEffectContents,
        useEffectUseCase,
        useEffectDocs,
      } = Effect()
      const hookDetails = {
        HookComponent: UseEffectComponent,
        contents: useEffectContents,
        useCase: useEffectUseCase,
        Docs: useEffectDocs,
        Name: hookId,
      }
      content = displayHookContents(hookDetails)
      break
    case "useContext":
      const {
        UseContextComponent,
        useContextContents,
        useContextUseCase,
        useContextDocs,
      } = Context()
      const useContextDetails = {
        HookComponent: UseContextComponent,
        contents: useContextContents,
        useCase: useContextUseCase,
        Docs: useContextDocs,
        Name: hookId,
      }
      content = displayHookContents(useContextDetails)
      break
    case "useRef":
      const { UseRefComponent, useRefContents, useRefUseCase, useRefDocs } =
        Ref()
      const useRefDetails = {
        HookComponent: UseRefComponent,
        contents: useRefContents,
        useCase: useRefUseCase,
        Docs: useRefDocs,
        Name: hookId,
      }
      content = displayHookContents(useRefDetails)
      break
    default:
      content = <div>{hookId}</div>
  }
  return (
    <div className="max-w-screen min-w-full my-auto container mb-10">
      <div className="flex justify-between sm:flex-row flex-col lg:mx-40">
        <h1 className="loud-text">{hookId}</h1>
        <button className="md:inline hidden">
          <Link className="purpleButtons text-xl font-bold" href="/Beginner">
            ⮌ Beginner Hooks
          </Link>
        </button>
      </div>
      {content}
    </div>
  )
}

function displayHookContents(hookDetails: HookCardType) {
  return (
    <div className="min-w-[75%] max-w-7xl mx-auto flex flex-col">
      <div className="mb-8 flex flex-col">
        <h2 className="text-2xl font-semibold mb-2">Use Cases:</h2>
        <div className="mx-auto bg-white font-light p-4 rounded-md text-lg w-full text-black">
          {hookDetails.useCase?.map((useCases, index) => (
            <ul className="my-2" key={index}>
              {useCases}
            </ul>
          ))}
        </div>
      </div>
      <div className="my-4">
        <h2 className="text-2xl font-semibold mb-2">Component Code:</h2>
        <pre className="bg-white text-black p-4 rounded-md overflow-x-auto">
          <CodeBlock
            text={hookDetails.contents}
            language={"typescript"}
            showLineNumbers={true}
            theme={atomOneLight}
          />
        </pre>
      </div>
      <div className="mb-8 flex flex-col">
        <h2 className="text-2xl font-semibold mb-2">Live Demo:</h2>
        <div className="mx-auto bg-white text-black p-4 rounded-md text-lg w-full">
          <hookDetails.HookComponent />
        </div>
      </div>
      <span className="flex justify-center">
        <button>
          <Link
            className="purpleButtons text-xl font-bold"
            href={hookDetails.Docs}
            target="_blank"
          >
            <span className="sm:inline hidden"> ⮏ </span> {hookDetails.Name}{" "}
            Documentation
          </Link>
        </button>
      </span>
    </div>
  )
}

//⮏ arrows
//⮌
//↥
