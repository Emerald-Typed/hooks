import Link from "next/link"

const BeginnerPage = () => {
  const links = [
    { href: "/Beginner/useState", title: "useState" },
    { href: "/Beginner/useEffect", title: "useEffect" },
    { href: "/Beginner/useContext", title: "useContext" },
    { href: "/Beginner/useRef", title: "useRef" },
  ]
  return (
    <div className="mx-auto min-w-[60%] lg:max-w-7xl my-auto">
      <h1 className="loud-text">Beginner Hooks</h1>
      <div className="border-b p-4 border-emerald-400">
        <ul className="flex flex-col font-medium gap-8 text-3xl">
          {links.map((link, index) => (
            <li key={index}>
              <Link href={link.href}>{link.title}</Link>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}

export default BeginnerPage
